package pl.polsl.administrationweb.entity;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Book;
import pl.polsl.administrationweb.entity.Film;
import pl.polsl.administrationweb.entity.Game;
import pl.polsl.administrationweb.entity.Genre;
import pl.polsl.administrationweb.entity.Rating;
import pl.polsl.administrationweb.entity.Review;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Position.class)
public class Position_ { 

    public static volatile CollectionAttribute<Position, Genre> genreCollection;
    public static volatile SingularAttribute<Position, Double> averageRating;
    public static volatile CollectionAttribute<Position, Game> gameCollection;
    public static volatile CollectionAttribute<Position, Film> filmCollection;
    public static volatile SingularAttribute<Position, Short> accepted;
    public static volatile SingularAttribute<Position, Integer> id;
    public static volatile SingularAttribute<Position, String> title;
    public static volatile CollectionAttribute<Position, Book> bookCollection;
    public static volatile SingularAttribute<Position, BigInteger> premiere;
    public static volatile SingularAttribute<Position, String> graphic;
    public static volatile CollectionAttribute<Position, Review> reviewCollection;
    public static volatile CollectionAttribute<Position, Rating> ratingCollection;

}