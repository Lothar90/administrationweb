package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Director;
import pl.polsl.administrationweb.entity.Position;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Film.class)
public class Film_ { 

    public static volatile SingularAttribute<Film, Director> directorId;
    public static volatile SingularAttribute<Film, Position> positionId;
    public static volatile SingularAttribute<Film, Integer> id;

}