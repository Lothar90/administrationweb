package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Game;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Publisher.class)
public class Publisher_ { 

    public static volatile CollectionAttribute<Publisher, Game> gameCollection;
    public static volatile SingularAttribute<Publisher, String> name;
    public static volatile SingularAttribute<Publisher, Integer> id;

}