package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Author;
import pl.polsl.administrationweb.entity.Position;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Book.class)
public class Book_ { 

    public static volatile SingularAttribute<Book, Position> positionId;
    public static volatile SingularAttribute<Book, Integer> id;
    public static volatile SingularAttribute<Book, Author> authorId;

}