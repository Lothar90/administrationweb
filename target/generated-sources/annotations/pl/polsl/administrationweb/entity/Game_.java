package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Company;
import pl.polsl.administrationweb.entity.Position;
import pl.polsl.administrationweb.entity.Publisher;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Game.class)
public class Game_ { 

    public static volatile SingularAttribute<Game, Company> companyId;
    public static volatile SingularAttribute<Game, Publisher> publisherId;
    public static volatile SingularAttribute<Game, Position> positionId;
    public static volatile SingularAttribute<Game, Integer> id;

}