package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Rating;
import pl.polsl.administrationweb.entity.Review;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, String> mail;
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> login;
    public static volatile CollectionAttribute<User, Review> reviewCollection;
    public static volatile CollectionAttribute<User, Rating> ratingCollection;

}