package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Position;
import pl.polsl.administrationweb.entity.User;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Rating.class)
public class Rating_ { 

    public static volatile SingularAttribute<Rating, Position> positionId;
    public static volatile SingularAttribute<Rating, Integer> id;
    public static volatile SingularAttribute<Rating, Double> value;
    public static volatile SingularAttribute<Rating, User> userId;

}