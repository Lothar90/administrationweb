package pl.polsl.administrationweb.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import pl.polsl.administrationweb.entity.Position;
import pl.polsl.administrationweb.entity.User;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-04T21:12:44")
@StaticMetamodel(Review.class)
public class Review_ { 

    public static volatile SingularAttribute<Review, Position> positionId;
    public static volatile SingularAttribute<Review, String> review;
    public static volatile SingularAttribute<Review, Integer> id;
    public static volatile SingularAttribute<Review, User> userId;

}