/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.model;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.polsl.administrationweb.entity.User;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "loggedUser")
@SessionScoped
public class LoggedUser implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private String login;
    private String password;

    @PostConstruct
    public void init() {

    }
    
    public String loginUser() {
        User temp = (User) em.createNamedQuery("User.findByLogin").setParameter("login", login).getSingleResult();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if(login.equals("admin") && Integer.toString(password.hashCode()).equals(temp.getPassword())) {
            facesContext.getExternalContext().getSessionMap().put("user", temp);
            return "/index?faces-redirect=true";
        } else {
            facesContext.addMessage(null, new FacesMessage("Podaj dane administratora, aby się zalogować"));
            return null;
        }
    }
    
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/login?faces-redirect=true";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    
}
