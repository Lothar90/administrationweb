package pl.polsl.administrationweb.servlets;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.polsl.administrationweb.entity.Position;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rafał Swoboda
 */
@WebServlet("/image/*")
public class ImageServlet extends HttpServlet {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id = Long.valueOf(request.getPathInfo().substring(1));
        Position position = (Position) em.createNamedQuery("Position.findById").setParameter("id", id).getSingleResult();
        if (position.getGraphic() == null) {
            try {
                BufferedImage bufferedImg = new BufferedImage(90, 25, BufferedImage.TYPE_INT_RGB);
                Graphics2D g2 = bufferedImg.createGraphics();
                g2.drawString("No image added", 0, 10);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(bufferedImg, "png", os);
                response.setContentType(getServletContext().getMimeType("image/png"));
                response.setContentLength(os.toByteArray().length);
                response.getOutputStream().write(os.toByteArray());
            } catch (IOException ex) {
                Logger.getLogger(Position.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                File file = new File(position.getGraphic());
                BufferedImage image = ImageIO.read(file);
                BufferedImage resizedImage = new BufferedImage(100, 140, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = resizedImage.createGraphics();
                g.drawImage(image, 0, 0, 100, 140, null);
                g.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(resizedImage, "png", baos);
                response.setContentType(getServletContext().getMimeType("image/png"));
                response.setContentLength(baos.toByteArray().length);
                response.getOutputStream().write(baos.toByteArray());
            } catch (IOException ex) {
                Logger.getLogger(Position.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
