/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;
import pl.polsl.administrationweb.entity.Author;
import pl.polsl.administrationweb.entity.Book;
import pl.polsl.administrationweb.entity.Genre;
import pl.polsl.administrationweb.entity.Position;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "books")
@ViewScoped
public class BooksWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Book> books;
    private Book selectedBook;
    private List<Author> authors;
    private List<Genre> genres;
    private int bookReviewsId;
    private UploadedFile file;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        books = em.createNamedQuery("Book.findAll").getResultList();
        genres = em.createNamedQuery("Genre.findAll").getResultList();
        authors = em.createNamedQuery("Author.findAll").getResultList();
    }

    public void edit(Book book) {
        selectedBook = book;
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editBookDialogWidget').show();");
    }

    public void applyEdit() {
        try {
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", selectedBook.getPositionId().getId()).getSingleResult();
            position.setGenreCollection(selectedBook.getPositionId().getGenreCollection());
            
            InputStream input = file.getInputstream();
            //File path = new File("C:\\Users\\Lothar\\Desktop\\inzynierka obrazki\\ksiazki", selectedBook.getId().toString());
            File path = new File("/home/zdjecia/ksiazki", selectedBook.getId().toString());
            OutputStream output = new FileOutputStream(path);
            try {
                IOUtils.copy(input, output);
            } finally {
                IOUtils.closeQuietly(input);
                IOUtils.closeQuietly(output);
            }
            
            position.setGraphic(path.getAbsolutePath());
            position.setPremiere(selectedBook.getPositionId().getPremiere());
            position.setTitle(selectedBook.getPositionId().getTitle());
            em.merge(position);
            selectedBook.setPositionId(position);
            em.merge(selectedBook);
            userTransaction.commit();
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException | IOException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
        printMessageForDialog(FacesMessage.SEVERITY_INFO, "Zedytowano książkę", selectedBook.getPositionId().getTitle(), "bookTableForm");
        closeDialog();
    }

    public void closeDialog() {
        books = em.createNamedQuery("Book.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editBookDialogWidget').hide();");
    }

    public void accept(Book book) {
        try {
            selectedBook = book;
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", selectedBook.getPositionId().getId()).getSingleResult();
            position.setAccepted(true);
            em.merge(position);
            userTransaction.commit();
            books = em.createNamedQuery("Book.findAll").getResultList();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Zaakceptowano", "Książka "
                    + selectedBook.getPositionId().getTitle() + " została zaakceptowana", "bookTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void decline(Book book) {
        try {
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", book.getPositionId().getId()).getSingleResult();
            position.setAccepted(true);
            em.remove(position);
            userTransaction.commit();
            books = em.createNamedQuery("Book.findAll").getResultList();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Odrzucono", "Książka "
                    + book.getPositionId().getTitle() + " została odrzucona", "bookTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String reviews(Book book) {
        bookReviewsId = book.getPositionId().getId();
        return "/data/book-reviews.xhtml?faces-redirect=true&includeViewParams=true";
    }

    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Book getSelectedBook() {
        return selectedBook;
    }

    public void setSelectedBook(Book selectedBook) {
        this.selectedBook = selectedBook;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public int getBookReviewsId() {
        return bookReviewsId;
    }

    public void setBookReviewsId(int bookReviewsId) {
        this.bookReviewsId = bookReviewsId;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Author getAuthor(String authorName) {
        if (authorName == null) {
            throw new IllegalArgumentException("no authory name provided");
        }
        for (Author a : authors) {
            if (authorName.equals(a.getName())) {
                return a;
            }
        }
        return null;
    }

    public Genre getGenre(String genreName) {
        if (genreName == null) {
            throw new IllegalArgumentException("no genre name provided");
        }
        for (Genre g : genres) {
            if (genreName.equals(g.getName())) {
                return g;
            }
        }
        return null;
    }
}
