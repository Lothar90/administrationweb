/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;
import pl.polsl.administrationweb.entity.Company;
import pl.polsl.administrationweb.entity.Game;
import pl.polsl.administrationweb.entity.Genre;
import pl.polsl.administrationweb.entity.Position;
import pl.polsl.administrationweb.entity.Publisher;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "games")
@ViewScoped
public class GamesWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Game> games;
    private Game selectedGame;
    private int gameReviewsId;
    private List<Publisher> publishers;
    private List<Company> companies;
    private List<Genre> genres;
    private UploadedFile file;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        games = em.createNamedQuery("Game.findAll").getResultList();
        publishers = em.createNamedQuery("Publisher.findAll").getResultList();
        companies = em.createNamedQuery("Company.findAll").getResultList();
        genres = em.createNamedQuery("Genre.findAll").getResultList();
    }

    public void edit(Game game) {
        selectedGame = game;
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editGameDialogWidget').show();");
    }

    public void accept(Game game) {
        try {
            selectedGame = game;
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", selectedGame.getPositionId().getId()).getSingleResult();
            position.setAccepted(true);
            em.merge(position);
            userTransaction.commit();
            games = em.createNamedQuery("Game.findAll").getResultList();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Zaakceptowano", "Gra " + selectedGame.getPositionId().getTitle() + " została zaakceptowana", "gameTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void decline(Game game) {
        try {
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", game.getPositionId().getId()).getSingleResult();
            em.remove(position);
            userTransaction.commit();
            games = em.createNamedQuery("Game.findAll").getResultList();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Odrzucono", "Gra " + game.getPositionId().getTitle() + " została odrzucona", "gameTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void applyEdit() {
        try {
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", selectedGame.getPositionId().getId()).getSingleResult();
            position.setGenreCollection(selectedGame.getPositionId().getGenreCollection());
            
            InputStream input = file.getInputstream();
            //File path = new File("C:\\Users\\Lothar\\Desktop\\inzynierka obrazki\\gry", selectedGame.getId().toString());
            File path = new File("/home/zdjecia/gry", selectedGame.getId().toString());
            OutputStream output = new FileOutputStream(path);
            try {
                IOUtils.copy(input, output);
            } finally {
                IOUtils.closeQuietly(input);
                IOUtils.closeQuietly(output);
            }
            
            position.setGraphic(path.getAbsolutePath());
            position.setPremiere(selectedGame.getPositionId().getPremiere());
            position.setTitle(selectedGame.getPositionId().getTitle());
            em.merge(position);
            selectedGame.setPositionId(position);
            em.merge(selectedGame);
            userTransaction.commit();
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException | IOException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
        printMessageForDialog(FacesMessage.SEVERITY_INFO, "Zedytowano grę", selectedGame.getPositionId().getTitle(), "gameTableForm");
        closeDialog();
    }

    public void closeDialog() {
        games = em.createNamedQuery("Game.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editGameDialogWidget').hide();");
    }

    public String reviews(Game game) {
        gameReviewsId = game.getPositionId().getId();
        return "/data/game-reviews.xhtml?faces-redirect=true&includeViewParams=true";
    }

    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public int getGameReviewsId() {
        return gameReviewsId;
    }

    public void setGameReviewsId(int gameReviewsId) {
        this.gameReviewsId = gameReviewsId;
    }

    public Game getSelectedGame() {
        return selectedGame;
    }

    public void setSelectedGame(Game selectedGame) {
        this.selectedGame = selectedGame;
    }

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Company getCompany(String companyName) {
        if (companyName == null) {
            throw new IllegalArgumentException("no company name provided");
        }
        for (Company c : companies) {
            if (companyName.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }

    public Publisher getPublisher(String publisherName) {
        if (publisherName == null) {
            throw new IllegalArgumentException("no publisher name provided");
        }
        for (Publisher p : publishers) {
            if (publisherName.equals(p.getName())) {
                return p;
            }
        }
        return null;
    }

    public Genre getGenre(String genreName) {
        if (genreName == null) {
            throw new IllegalArgumentException("no genre name provided");
        }
        for (Genre g : genres) {
            if (genreName.equals(g.getName())) {
                return g;
            }
        }
        return null;
    }
}
