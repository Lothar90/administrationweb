/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;
import pl.polsl.administrationweb.entity.Genre;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "genres")
@ViewScoped
public class GenresWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Genre> genres;
    private Genre newGenre;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        genres = em.createNamedQuery("Genre.findAll").getResultList();
    }
    
    public void openAddDialog() {
        this.newGenre = new Genre();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addGenreDialogWidget').show();");
    }
    
    public void add() {
        try {
            userTransaction.begin();
            em.persist(newGenre);
            userTransaction.commit();
            em.getEntityManagerFactory().getCache().evictAll();
            genres.clear();
            genres.addAll(em.createNamedQuery("Genre.findAll").getResultList());
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Dodano", "Dodano nowy gatunek", "authorTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException | SecurityException
                | IllegalStateException ex) {
            Logger.getLogger(GameReviewsWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeDialog() {
        em.getEntityManagerFactory().getCache().evictAll();
        genres = em.createNamedQuery("Genre.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addGenreDialogWidget').hide();");
    }
    
    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public Genre getNewGenre() {
        return newGenre;
    }

    public void setNewGenre(Genre newGenre) {
        this.newGenre = newGenre;
    }

}
