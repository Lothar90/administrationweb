/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "menu")
@SessionScoped
public class MenuView implements Serializable {

    private MenuModel model;

    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();

        //First submenu
        DefaultSubMenu firstSubmenu = new DefaultSubMenu("Zarządzaj danymi");

        DefaultMenuItem item = new DefaultMenuItem("Gry");
        item.setCommand("/data/games?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Książki");
        item.setCommand("/data/books?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Filmy");
        item.setCommand("/data/films?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Gatunki");
        item.setCommand("/data/genres?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Autorzy");
        item.setCommand("/data/authors?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Reżyserzy");
        item.setCommand("/data/directors?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Wydawcy");
        item.setCommand("/data/publishers?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        item = new DefaultMenuItem("Producenci");
        item.setCommand("/data/companies?faces-redirect=true");
        item.setIcon("ui-icon-home");
        firstSubmenu.addElement(item);

        model.addElement(firstSubmenu);

        //Second submenu
        DefaultSubMenu secondSubmenu = new DefaultSubMenu("Zarządzaj kontami");

        item = new DefaultMenuItem("Użytkownicy");
        item.setCommand("/data/users?faces-redirect=true");
        item.setIcon("ui-icon-disk");
        secondSubmenu.addElement(item);

        item = new DefaultMenuItem("Wylogowanie");
        item.setIcon("ui-icon-close");
        item.setCommand("#{loggedUser.logout()}");
        secondSubmenu.addElement(item);

        model.addElement(secondSubmenu);
    }

    public MenuModel getModel() {
        return model;
    }
}
