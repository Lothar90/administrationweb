/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;
import pl.polsl.administrationweb.entity.Publisher;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "publishers")
@ViewScoped
public class PublishersWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Publisher> publishers;
    private Publisher newPublisher;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        publishers = em.createNamedQuery("Publisher.findAll").getResultList();
    }
    
    public void openAddDialog() {
        this.newPublisher = new Publisher();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addPublisherDialogWidget').show();");
    }
    
    public void add() {
        try {
            userTransaction.begin();
            em.persist(newPublisher);
            userTransaction.commit();
            em.getEntityManagerFactory().getCache().evictAll();
            publishers.clear();
            publishers.addAll(em.createNamedQuery("Publisher.findAll").getResultList());
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Dodano", "Dodano nowego wydawcę", "authorTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException | SecurityException
                | IllegalStateException ex) {
            Logger.getLogger(GameReviewsWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeDialog() {
        em.getEntityManagerFactory().getCache().evictAll();
        publishers = em.createNamedQuery("Publisher.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addPublisherDialogWidget').hide();");
    }
    
    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    public Publisher getNewPublisher() {
        return newPublisher;
    }

    public void setNewPublisher(Publisher newPublisher) {
        this.newPublisher = newPublisher;
    }

}
