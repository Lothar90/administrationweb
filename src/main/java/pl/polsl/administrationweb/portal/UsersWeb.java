/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;
import pl.polsl.administrationweb.entity.User;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "users")
@ViewScoped
public class UsersWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<User> users;
    private User selectedUser;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        users = em.createNamedQuery("User.findAll").getResultList();
        for (User u : users) {
            if (u.getLogin().equals("admin")) {
                users.remove(u);
                break;
            }
        }
    }

    public void userChange(User user) {
        selectedUser = user;
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editPasswordDialogWidget').show();");
    }
    
    public void changePassword() {
        try {
            userTransaction.begin();
            String password = selectedUser.getPassword();
            selectedUser.setPassword(Integer.toString(password.hashCode()));
            em.merge(selectedUser);
            userTransaction.commit();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Hasło zmienione", "", "userTableForm");
            closeDialog();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            Logger.getLogger(UsersWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeDialog() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editPasswordDialogWidget').hide();");
    }

    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }
    
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }
}
