/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.UploadedFile;
import pl.polsl.administrationweb.entity.Director;
import pl.polsl.administrationweb.entity.Film;
import pl.polsl.administrationweb.entity.Genre;
import pl.polsl.administrationweb.entity.Position;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "films")
@ViewScoped
public class FilmsWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Film> films;
    private Film selectedFilm;
    private List<Director> directors;
    private List<Genre> genres;
    private int filmReviewsId;
    private UploadedFile file;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        films = em.createNamedQuery("Film.findAll").getResultList();
        genres = em.createNamedQuery("Genre.findAll").getResultList();
        directors = em.createNamedQuery("Director.findAll").getResultList();
    }

    public void edit(Film film) {
        selectedFilm = film;
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editFilmDialogWidget').show();");
    }

    public void applyEdit() {
        try {
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", selectedFilm.getPositionId().getId()).getSingleResult();
            position.setGenreCollection(selectedFilm.getPositionId().getGenreCollection());

            InputStream input = file.getInputstream();
            //File path = new File("C:\\Users\\Lothar\\Desktop\\inzynierka obrazki\\filmy", selectedFilm.getId().toString());
            File path = new File("/home/zdjecia/filmy", selectedFilm.getId().toString());
            OutputStream output = new FileOutputStream(path);
            try {
                IOUtils.copy(input, output);
            } finally {
                IOUtils.closeQuietly(input);
                IOUtils.closeQuietly(output);
            }

            position.setGraphic(path.getAbsolutePath());
            position.setPremiere(selectedFilm.getPositionId().getPremiere());
            position.setTitle(selectedFilm.getPositionId().getTitle());
            em.merge(position);
            selectedFilm.setPositionId(position);
            em.merge(selectedFilm);
            userTransaction.commit();
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException | IOException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
        printMessageForDialog(FacesMessage.SEVERITY_INFO, "Zedytowano film", selectedFilm.getPositionId().getTitle(), "filmTableForm");
        closeDialog();
    }

    public void closeDialog() {
        films = em.createNamedQuery("Film.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('editFilmDialogWidget').hide();");
    }

    public void accept(Film film) {
        try {
            selectedFilm = film;
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", selectedFilm.getPositionId().getId()).getSingleResult();
            position.setAccepted(true);
            em.merge(position);
            userTransaction.commit();
            films = em.createNamedQuery("Film.findAll").getResultList();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Zaakceptowano", "Film "
                    + selectedFilm.getPositionId().getTitle() + " został zaakceptowany", "filmTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void decline(Film film) {
        try {
            userTransaction.begin();
            Position position = (Position) em.createNamedQuery("Position.findById")
                    .setParameter("id", film.getPositionId().getId()).getSingleResult();
            em.remove(position);
            userTransaction.commit();
            films = em.createNamedQuery("Film.findAll").getResultList();
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Odrzucono", "Film "
                    + film.getPositionId().getTitle() + " został odrzucony", "filmTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException
                | SecurityException | IllegalStateException ex) {
            Logger.getLogger(GamesWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String reviews(Film film) {
        filmReviewsId = film.getPositionId().getId();
        return "/data/film-reviews.xhtml?faces-redirect=true&includeViewParams=true";
    }

    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

    public int getFilmReviewsId() {
        return filmReviewsId;
    }

    public void setFilmReviewsId(int filmReviewsId) {
        this.filmReviewsId = filmReviewsId;
    }

    public Film getSelectedFilm() {
        return selectedFilm;
    }

    public void setSelectedFilm(Film selectedFilm) {
        this.selectedFilm = selectedFilm;
    }

    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Director getDirector(String directorName) {
        if (directorName == null) {
            throw new IllegalArgumentException("no authory name provided");
        }
        for (Director d : directors) {
            if (directorName.equals(d.getName())) {
                return d;
            }
        }
        return null;
    }

    public Genre getGenre(String genreName) {
        if (genreName == null) {
            throw new IllegalArgumentException("no genre name provided");
        }
        for (Genre g : genres) {
            if (genreName.equals(g.getName())) {
                return g;
            }
        }
        return null;
    }
}
