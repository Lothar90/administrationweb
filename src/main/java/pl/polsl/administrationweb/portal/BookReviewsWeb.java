/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import pl.polsl.administrationweb.entity.Position;
import pl.polsl.administrationweb.entity.Rating;
import pl.polsl.administrationweb.entity.Review;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "bookReviews")
@ViewScoped
public class BookReviewsWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private int positionId;
    private List<Review> reviews;
    private Position position;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        reviews = new ArrayList<>();
        positionId = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("positionID"));
        position = (Position) em.createNamedQuery("Position.findById").setParameter("id", positionId).getSingleResult();
        reviews.addAll(position.getReviewCollection());
    }

    public void delete(Review review) {
        try {
            userTransaction.begin();
            Review removableReview = (Review) em.createNamedQuery("Review.findById").setParameter("id", review.getId()).getSingleResult();
            List<Rating> ratings = (List<Rating>) position.getRatingCollection();
            double avg_rating = 0;
            for(Rating r : ratings) {
                if(Objects.equals(r.getPositionId().getId(), removableReview.getPositionId().getId()) 
                        && Objects.equals(r.getUserId().getId(), removableReview.getUserId().getId())) {
                    Rating ratingToDelete = (Rating) em.createNamedQuery("Rating.findById").setParameter("id", r.getId()).getSingleResult();
                    em.remove(ratingToDelete);
                } else
                    avg_rating += r.getValue();
            }
            Position actualizePosition = (Position) em.createNamedQuery("Position.findById").setParameter("id", positionId).getSingleResult();
            if(ratings.size() - 1 > 0)
                actualizePosition.setAverageRating(avg_rating /(ratings.size()-1));
            else
                actualizePosition.setAverageRating(0.0);
            em.merge(actualizePosition);
            em.remove(removableReview);
            userTransaction.commit();
            em.getEntityManagerFactory().getCache().evictAll();
            position = (Position) em.createNamedQuery("Position.findById").setParameter("id", positionId).getSingleResult();
            reviews.clear();
            reviews.addAll(position.getReviewCollection());
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Usunięto", "Wybrana recenzja została usunięta", "bookReviewTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException | SecurityException
                | IllegalStateException ex) {
            Logger.getLogger(GameReviewsWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
