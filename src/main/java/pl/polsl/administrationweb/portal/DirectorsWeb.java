/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;
import pl.polsl.administrationweb.entity.Director;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "directors")
@ViewScoped
public class DirectorsWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Director> directors;
    private Director newDirector;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        directors = em.createNamedQuery("Director.findAll").getResultList();
    }
    
    public void openAddDialog() {
        this.newDirector = new Director();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addDirectorDialogWidget').show();");
    }
    
    public void add() {
        try {
            userTransaction.begin();
            em.persist(newDirector);
            userTransaction.commit();
            em.getEntityManagerFactory().getCache().evictAll();
            directors.clear();
            directors.addAll(em.createNamedQuery("Director.findAll").getResultList());
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Dodano", "Dodano nowego reżysera", "authorTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException | SecurityException
                | IllegalStateException ex) {
            Logger.getLogger(GameReviewsWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeDialog() {
        em.getEntityManagerFactory().getCache().evictAll();
        directors = em.createNamedQuery("Director.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addDirectorDialogWidget').hide();");
    }
    
    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    public Director getNewDirector() {
        return newDirector;
    }

    public void setNewDirector(Director newDirector) {
        this.newDirector = newDirector;
    }

}
