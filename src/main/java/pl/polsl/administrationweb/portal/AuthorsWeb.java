/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.portal;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;
import pl.polsl.administrationweb.entity.Author;

/**
 *
 * @author Rafał Swoboda
 */
@ManagedBean(name = "authors")
@ViewScoped
public class AuthorsWeb implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private List<Author> authors;
    private Author newAuthor;
    @Resource
    private UserTransaction userTransaction;

    @PostConstruct
    public void init() {
        em.getEntityManagerFactory().getCache().evictAll();
        authors = em.createNamedQuery("Author.findAll").getResultList();
    }

    public void openAddDialog() {
        this.newAuthor = new Author();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addAuthorDialogWidget').show();");
    }

    public void add() {
        try {
            userTransaction.begin();
            em.persist(newAuthor);
            userTransaction.commit();
            em.getEntityManagerFactory().getCache().evictAll();
            authors.clear();
            authors.addAll(em.createNamedQuery("Author.findAll").getResultList());
            printMessageForDialog(FacesMessage.SEVERITY_INFO, "Dodano", "Dodano nowego autora", "authorTableForm");
        } catch (NotSupportedException | SystemException | RollbackException
                | HeuristicMixedException | HeuristicRollbackException | SecurityException
                | IllegalStateException ex) {
            Logger.getLogger(GameReviewsWeb.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeDialog() {
        em.getEntityManagerFactory().getCache().evictAll();
        authors = em.createNamedQuery("Author.findAll").getResultList();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('addAuthorDialogWidget').hide();");
    }

    private void printMessageForDialog(FacesMessage.Severity severity, String title, String messageContent, String formIdUpdate) {
        FacesMessage msg = new FacesMessage(severity, title, messageContent);
        FacesContext.getCurrentInstance().addMessage(formIdUpdate, msg);
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Author getNewAuthor() {
        return newAuthor;
    }

    public void setNewAuthor(Author newAuthor) {
        this.newAuthor = newAuthor;
    }
}
