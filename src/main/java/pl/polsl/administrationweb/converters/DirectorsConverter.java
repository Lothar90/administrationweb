/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.converters;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import pl.polsl.administrationweb.entity.Director;
import pl.polsl.administrationweb.portal.FilmsWeb;

/**
 *
 * @author Rafał Swoboda
 */
@FacesConverter("directorsConverter")
public class DirectorsConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String directorName) {
        ValueExpression vex =
                context.getApplication().getExpressionFactory()
                        .createValueExpression(context.getELContext(),
                                "#{films}", FilmsWeb.class);

        FilmsWeb filmsWeb = (FilmsWeb)vex.getValue(context.getELContext());
        return filmsWeb.getDirector(directorName);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object director) {
        return ((Director)director).getName();
    }
    
}
