/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.administrationweb.converters;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import pl.polsl.administrationweb.entity.Genre;
import pl.polsl.administrationweb.portal.BooksWeb;

/**
 *
 * @author Rafał Swoboda
 */
@FacesConverter("bookGenresConverter")
public class BookGenresConverter implements Converter{
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String genreName) {
        ValueExpression vex =
                context.getApplication().getExpressionFactory()
                        .createValueExpression(context.getELContext(),
                                "#{books}", BooksWeb.class);

        BooksWeb booksWeb = (BooksWeb)vex.getValue(context.getELContext());
        return booksWeb.getGenre(genreName);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object genre) {
        return (genre instanceof Genre) ? ((Genre) genre).getName() : null;
    }
}
